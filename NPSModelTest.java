/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       5/7/2018
 * Filename:   NPSModelTest.java
 * Purpose:    Test file for testing components of the final project.   
 */

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * Test the Calculator class
 */
public class NPSModelTest
{ 

    // Test to see if 
  @Test
  public void testGetNPS1()
  {
    NPSModel npsModel = new NPSModel();
    assertEquals(true, npsModel.getNPS("NV"));
  }

  @Test
  public void testGetNPS2()
  {
    NPSModel npsModel = new NPSModel();
    assertEquals(false, npsModel.getNPS("AA"));
  }

  @Test
  public void testGetParkName()
  {
    NPSModel npsModel = new NPSModel();
    assertEquals(true, npsModel.getNPS("NV"));
    assertEquals("Great Basin National Park", npsModel.getParkName(2));
  }

  @Test
  public void testGetURL()
  {
    NPSModel npsModel = new NPSModel();
    assertEquals(true, npsModel.getNPS("NV"));
    assertEquals("https://www.nps.gov/grba/index.htm", npsModel.getParkURL(2));
  }


}
