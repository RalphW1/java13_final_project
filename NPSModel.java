/**
 * Created by Tim McGowen on 3/22/2017
 *
 * Model to get National Park data based on state.
 */

/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       5/7/2018
 * Filename:   NPSModel.java
 * Purpose:    Model for the final project of extracting National Park Service  
 */

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;
import java.lang.String;


public class NPSModel {
   private JsonElement jse;
   private final String npsAPI_Key = "PSWKsHy2Ff8q5e0nRUyuZ2qjDgmrCY8qaSft4hLV";

  public boolean getNPS(String stateCode)
  {
    
    try
    {
      URL npsURL = new URL("https://developer.nps.gov/api/v1/parks?" +
          "stateCode=" + stateCode + "&api_key=" + npsAPI_Key + "&fields=images");

      // Open connection
      InputStream is = npsURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // System.out.println(jse);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the stateCode was valid.
    return isValid(stateCode);
  }

   public boolean isValid(String stateCode)
   {
      Integer errorCode = 0;

    // If the stateCode is not valid we will get a integer 0 for the errorCode 
      try {
         errorCode = jse.getAsJsonObject().get("total").getAsInt();
      }

      catch (java.lang.NullPointerException npe)
      {
         // We did not see error so this is a valid zip
      }
      
      if (errorCode != 0 && stateCode.length() == 2) {
         // System.out.println("No Error");
         return true;
         }
      else 
         return false;
      
   }
   
   // Method for extracting the park's directions from the JSON data using a integer to point to the park
   public String getDirections(int arrayLocation)
    {
       JsonArray dataArray = new JsonArray();
       dataArray = jse.getAsJsonObject().get("data").getAsJsonArray();
       String strDirections = dataArray.get(arrayLocation).getAsJsonObject().get("directionsInfo").getAsString();
       return strDirections;
     }

   // Method for extracting the real park name from the JSON data
   public String getParkName(int arrayLocation)
    {
       JsonArray dataArray = new JsonArray();
       dataArray = jse.getAsJsonObject().get("data").getAsJsonArray();
       String strFullName = dataArray.get(arrayLocation).getAsJsonObject().get("fullName").getAsString();
       return strFullName;
     }
     
   // Method for extracting whether it's a National Park or Monument from the JSON data
   public String getParkDesignation(int arrayLocation)
    {
       JsonArray dataArray = new JsonArray();
       dataArray = jse.getAsJsonObject().get("data").getAsJsonArray();
       String strParkDesignation = dataArray.get(arrayLocation).getAsJsonObject().get("designation").getAsString();
       return strParkDesignation;
     }

   // Method for finding the total record count of all the items in the JSON data
   public int getRecordCount()
   { 
      return jse.getAsJsonObject().get("total").getAsInt();
   }

   // Method for extracting the park's URL      
   public String getParkURL(int arrayLocation)
   {
      JsonArray dataArray = new JsonArray();
      dataArray = jse.getAsJsonObject().get("data").getAsJsonArray();
      String strParkURL = dataArray.get(arrayLocation).getAsJsonObject().get("url").getAsString();
      return strParkURL;
   }

   // Method for extracting weather info for the park or monument
   public String getWeatherInfo(int arrayLocation)
   {
      JsonArray dataArray = new JsonArray();
      dataArray = jse.getAsJsonObject().get("data").getAsJsonArray();
      String strWeatherInfo = dataArray.get(arrayLocation).getAsJsonObject().get("weatherInfo").getAsString();
      return strWeatherInfo;
   }
   
   public Image getParkImage(int arrayLocation, int imageLocation)
   {
      JsonArray dataArray = new JsonArray();
      JsonArray imageArray = new JsonArray();
      dataArray = jse.getAsJsonObject().get("data").getAsJsonArray();
      imageArray = dataArray.get(arrayLocation).getAsJsonObject().get("images").getAsJsonArray();
      String iconURL = imageArray.get(imageLocation).getAsJsonObject().get("url").getAsString();
      // System.out.println(iconURL);
      return new Image(iconURL);
   }
    
}
