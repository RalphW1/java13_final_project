/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This code initiates the View and Controller.
 */

/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       5/7/2018
 * Filename:   NPSMain.java
 * Purpose:    Controller for the MVC version of extracting National Park Service Data  
 */


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class NPSMain extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("./NPSView.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("NPS  - Ralph Williams");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
