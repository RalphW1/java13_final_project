/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */

/*
 * Name:       modified by Ralph Williams
 * Course:     CSCI0013
 * Date:       5/7/2018
 * Filename:   NPSController.java
 * Purpose:    Controller for the MVC version of extracting National Parks Data  
 */ 


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.*;
import javafx.collections.FXCollections;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class NPSController implements Initializable {

   final String strNatPark = "National Park";
   final String strNatMonument = "National Monument";
   public int[] itemPointer = new int[50];
   public int numItems = 0;
   public int numRecords = 0;
   boolean fillDDmenuPressed = false;
   public boolean radioBtnPressed = true;
   
   @FXML
   private TextField txtStateCode;
   
   @FXML
   private TextField txtURL;
   
   @FXML
   private TextArea txtDirections;
    
   @FXML
   private RadioButton btnNatParks;
  
   @FXML
   private RadioButton btnNatMonuments;

   public boolean natParks;
  
   @FXML
   private Button btnFillDDMenu;
  
   @FXML
   private ChoiceBox cboxDDMenu = new ChoiceBox();
  
   @FXML
   private Button btnGo;
  
   @FXML
   private Image imgParkIcon;
  
   @FXML
   private TextArea txtWeatherInfo;

   //private Label lblWeatherInfo;
  
   @FXML
   private Label lblParkTitle;
  
   @FXML
   private ImageView iconNPS;

   @FXML
   private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
   
      NPSModel nps_model = new NPSModel();

    // Has the National Parks Radio button been pressed?
    if (e.getSource() == btnNatParks)
    {     
      natParks = true;
      radioBtnPressed = true;
    }
    // Has the National Monuments Radio button been pressed?
    if (e.getSource() == btnNatMonuments)
    {
      natParks = false;
      radioBtnPressed = true;
    }  
    
    // Has the btnFillDDMenu button been pressed?
    if (e.getSource() == btnFillDDMenu)
    {
      radioBtnPressed = false;
      fillDDmenuPressed = true;
          
      String stateCode = txtStateCode.getText();
                             
      if (nps_model.getNPS(stateCode))
      {
            
         numRecords = nps_model.getRecordCount();
       
         numItems = 0;
         
         // Loop through the JSON data to count the number of parks or monuments
         for (int i = 0; i < numRecords; i++) {
             if (natParks == true) {
                if (nps_model.getParkDesignation(i).equals(strNatPark)) {
                  itemPointer[numItems] = i;
                  numItems++;
                  }
               }
             else {    
               if (nps_model.getParkDesignation(i).equals(strNatMonument)) {
                   itemPointer[numItems] = i;
                   numItems++;
               }
             }
          }
                   
         // System.out.println("The number of records of interest is " + numItems);
                      
         // Clear all the displayed fields if they're set from a previous query                 
         txtDirections.setText("");         
         lblParkTitle.setText(""); 
         txtWeatherInfo.setText("");  
         txtURL.setText(""); 
         iconNPS.setImage(null);
         
            
         // Fill the drop down menu with the items that have been identified
         // The current application can only handle the first seven items
         if (numItems == 0) {
            txtDirections.setText("There are no items in your query for the state selected"); 
            iconNPS.setImage(new Image("badStateCode.png"));
            cboxDDMenu.setItems(FXCollections.observableArrayList(""));
         }
         else if (numItems == 1)
            cboxDDMenu.setItems(FXCollections.observableArrayList(nps_model.getParkName(itemPointer[0])));
         else if (numItems == 2)
            cboxDDMenu.setItems(FXCollections.observableArrayList
            (nps_model.getParkName(itemPointer[0]),nps_model.getParkName(itemPointer[1])));
         else if (numItems == 3)
            cboxDDMenu.setItems(FXCollections.observableArrayList
            (nps_model.getParkName(itemPointer[0]),nps_model.getParkName(itemPointer[1]),nps_model.getParkName(itemPointer[2])));          
         else if (numItems == 4)  
            cboxDDMenu.setItems(FXCollections.observableArrayList
            (nps_model.getParkName(itemPointer[0]),nps_model.getParkName(itemPointer[1]),nps_model.getParkName(itemPointer[2]),
            nps_model.getParkName(itemPointer[3])));          
         else if (numItems == 5) 
            cboxDDMenu.setItems(FXCollections.observableArrayList
            (nps_model.getParkName(itemPointer[0]),nps_model.getParkName(itemPointer[1]),nps_model.getParkName(itemPointer[2]),
            nps_model.getParkName(itemPointer[3]),nps_model.getParkName(itemPointer[4])));         
         else if (numItems == 6)    
            cboxDDMenu.setItems(FXCollections.observableArrayList   
            (nps_model.getParkName(itemPointer[0]),nps_model.getParkName(itemPointer[1]),nps_model.getParkName(itemPointer[2]),   
            nps_model.getParkName(itemPointer[3]),nps_model.getParkName(itemPointer[4]),nps_model.getParkName(itemPointer[5])));  
          else if (numItems >= 7)    
             cboxDDMenu.setItems(FXCollections.observableArrayList   
             (nps_model.getParkName(itemPointer[0]),nps_model.getParkName(itemPointer[1]),nps_model.getParkName(itemPointer[2]),   
             nps_model.getParkName(itemPointer[3]),nps_model.getParkName(itemPointer[4]),nps_model.getParkName(itemPointer[5]),
             nps_model.getParkName(itemPointer[6])));
         
      }
      else
      {
        txtDirections.setText("Invalid State Code");
        iconNPS.setImage(new Image("badStateCode.png"));
        txtURL.setText("");
        txtWeatherInfo.setText("");
        cboxDDMenu.setItems(FXCollections.observableArrayList(""));
      }
    }
    
    if (e.getSource() == btnGo) {
         
         radioBtnPressed = false;    

         
         if (numItems == 0 && fillDDmenuPressed == true && radioBtnPressed == false) {
            txtDirections.setText("There are no items in your query for the state selected");
            iconNPS.setImage(new Image("badStateCode.png"));
         }
         else if (fillDDmenuPressed == false || radioBtnPressed == true) {
            txtDirections.setText("Invalid operation, you must fill the menu before hitting the GO button, Please try again");
            iconNPS.setImage(new Image("badStateCode.png"));
            lblParkTitle.setText(""); 
            txtWeatherInfo.setText("");  
            txtURL.setText(""); 
            cboxDDMenu.setItems(FXCollections.observableArrayList(""));
         }
         else {
            
            // Wrap the text for the TextArea boxes    
            txtDirections.setWrapText(true);
            txtWeatherInfo.setWrapText(true);
            
            String stateCode = txtStateCode.getText();         
            int itemSelected = 0;
            
            // System.out.println("The value from the choice box when nothing is selected is");
            // System.out.println(cboxDDMenu.getValue());
            
            if (nps_model.getNPS(stateCode)) {
            
               if (cboxDDMenu.getValue() == null) {
                  txtDirections.setText("Invalid user operation, select a park/monument and try again ");               
                  iconNPS.setImage(new Image("badStateCode.png"));
               }
               else {                      
                  iconNPS.setImage(null);
                  String cboxValue = cboxDDMenu.getValue().toString();
                  // Need to reverse engineer the drop-down menu and find the index of the item of interest
                  for (int i = 0; i < numRecords; i++) {
                     if (nps_model.getParkName(i).equals(cboxValue)) 
                     itemSelected = i;
                  }       
                   // If we got to here, its a valid park/monument so print out the information on it to the user
                   iconNPS.setImage(nps_model.getParkImage(itemSelected, 0));
                   txtDirections.setText(nps_model.getDirections(itemSelected));       
                   lblParkTitle.setText(nps_model.getParkName(itemSelected));
                   txtWeatherInfo.setText(nps_model.getWeatherInfo(itemSelected));       
                   txtURL.setText(nps_model.getParkURL(itemSelected));
                  
               } // End of printing out valid data     
                        
              
            } // End of check whehter the state code is valid
         } // End of there are at least valid items in the query
         
         fillDDmenuPressed = false;
     } // End of btnGo code
    
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    
}


